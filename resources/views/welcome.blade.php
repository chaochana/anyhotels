<!DOCTYPE html>
<html>
    <head>
        <title>anyHotels</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
        <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.8.1/bootstrap-table.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .hotel-img {
              position: relative;
              width: 400px;
              height: 250px;
              /*overflow: hidden;*/
              padding: 4px;
            }
        </style>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row text-center">
                <div class="col-lg-12 text-center">
                    <h1>anyHotels</h1>
                    <?php
                        $response_json = file_get_contents("http://rates.asiawebdirect.com/asahi/hotels/en_US/dateless?dest=75&pageSize=100");
                        $response = json_decode($response_json);

                        $results = $response->results;

                        // $all_description = "";

                        // foreach ($results as $result) {
                        //     $all_description .= $result->description; 
                        // }

                        // $words = str_word_count($all_description, 1); // use this function if you only want ASCII
                        // // $words = utf8_str_word_count($all_description, 1); // use this function if you care about i18n

                        // $ignore_array = array('an','the','a','-','and','is','off','to','with','The','are','at','as');

                        // $frequency = array_count_values($words);

                        // arsort($frequency);

                        // $filtered_frequency = array_diff_key($frequency, array_flip($ignore_array));

                        // echo '<pre>';
                        // print_r($filtered_frequency);
                        // echo '</pre>';

                        foreach ($results as $result) {
                            // echo '<div style="text-align:center">';
                            // if (preg_match("/spa/i", $result->description)) {

                            // $myWords = array('villa','beach','pool','shopping');
                            $myWords = array('swimming pool');
                            $myWordString = join('|', $myWords);
                             
                            $myRegex = "/(^|[\s.,])(" . $myWordString . ")([\s.,]|$)/iu";

                            if ( preg_match_all($myRegex, $result->description, $match) ) {
                                echo '<img class="hotel-img" src="'.$result->image_url.'" width="300" alt="'.$result->name.'">';
                                echo '<h3>*YES-'.$result->name.'*</h3>';
                                echo '<h5><strong class="text-success" style="font-family:Arial;">';
                                $keywords_string = implode(",", $match[0]);
                                echo $keywords_string;
                                echo '</strong></h5>';
                                echo '<p><small>'.$result->description.'</small></p>';

                            } else {
                                // echo '<h3 class="text-danger">*BOOO-'.$result->name.'*</h3>';
                                // echo '<small class="text-danger">'.$result->description.'</small>';
                            }
                            
                            // echo $result->name;
                            // echo '</div>';
                        }
                    ?>
                </div>
            </div>
        </div>
    </body>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
</html>
